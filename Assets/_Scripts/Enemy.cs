﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Enemy : MonoBehaviour
{

    public int life = 3;
    public GameObject spawner;
    
    void Start()
    {

    }
    
    void Update()
    {
        
    }

    public void DealDemage(int demage)
    {
        life -= demage;
        if(life <= 0)
        {
            spawner.GetComponent<State_test>().enemies.Remove(gameObject);
            // Jakub's change: deleted EnemiesNumber variable from State_test script since after changes it was doing basically nothing
            //spawner.GetComponent<State_test>().EnemiesNumber--;
            Destroy(gameObject);
        }
    }
}
