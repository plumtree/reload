﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class State_test : MonoBehaviour
{
    [Header("Player and Look At objects")]
    [SerializeField] private GameObject dollycart;
    [SerializeField] private GameObject LookAtObj;
    [SerializeField] private GameObject ParentLookAtObj;
    [SerializeField] GameObject vCam;

    [Header("Enemies")]
    public List<GameObject> enemies;

    [Header("Other Settings")]
    // movestep might be added to change the speed of the player chaning targets
    //[SerializeField] float moveStep;
    [SerializeField] float stopFactor;
    [SerializeField] float startFactor;

    float newDistance;
    private bool isFighting = false;
    private float previousPlayerSpeed;
    private float previousLookAtSpeed;

    bool isMoving = false;

    void Start()
    {
        //Check if dollycart and LookAtObj are aligned in the inspector. If not exit the app.
        if(dollycart == null || LookAtObj == null)
        {
            Debug.LogError("Obj name " + gameObject.name + " :Dollycart and LookAt Object must be aligned!");
            UnityEditor.EditorApplication.isPlaying = false;
        }
        
        isFighting = false;
    }

    void Update()
    {
        // If fight starts set the speed of dollycart and lookatobj to 0 in order to stop the movement
        if (isFighting == true)
        {
            StopPlayerCart(stopFactor);
            ChangeLookAtPos();
        }
        // When enemies are killed destroy the trigger and set status to notfighting
        if (enemies.Count <= 0)
        {
            isFighting = false;
        }
        // When fight is over and no enemies are alive set the speed of both object back to default 1
        if (isFighting == false && enemies.Count == 0)
        {
            float tempDist = Vector3.Distance(Vector3.zero, vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition);
            //Debug.Log("actual lookAt: " + vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position + ", player pos: " + dollycart.GetComponent<Transform>().position + " ; parent lookAt: " + ParentLookAtObj.GetComponent<Transform>().position + "player pos: " + dollycart.GetComponent<Transform>().position);
            float dot = Vector3.Angle((vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.GetComponent<Transform>().TransformPoint(Vector3.zero) - dollycart.GetComponent<Transform>().position).normalized, (LookAtObj.GetComponent<Transform>().position - dollycart.GetComponent<Transform>().position).normalized);
            //float dot = Vector3.Dot((vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position - GetComponent<Transform>().position), (vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.GetComponentInParent<Transform>().position - GetComponent<Transform>().position));
            Debug.Log("dot: " + dot);

            //Debug.Log(Vector3.Distance(Vector3.zero, vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition));
            if (/*Vector3.Distance(Vector3.zero, vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition) <= 8f*/dot < 15f)
            {
               // Debug.Log("new test");
                vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition = Vector3.MoveTowards(vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition, Vector3.zero, (0.1f + tempDist) / 10);
                //vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition = Vector3.zero;
            }
            vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition = Vector3.MoveTowards(vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition, Vector3.zero, (0.1f + tempDist) / 50);
            
            //Debug.Log(Vector3.Distance(Vector3.zero, vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition));
            if (vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition == Vector3.zero && dollycart.GetComponent<CinemachineDollyCart>().m_Speed == 0f)
            {
                Debug.Log("testetstetst");
                vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.localPosition = Vector3.zero;
                StartPlayerCart(startFactor);

                if (dollycart.GetComponent<CinemachineDollyCart>().m_Speed == previousPlayerSpeed)
                {
                    Destroy(gameObject);
                }
                return;
            }
        }
    }

    // Called only when hitting the trigger
    private void OnTriggerEnter(Collider other)
    {
        // When the collider that's hit have the tag of Player set the status to isFighting
        if (other.tag == "Player")
        {
            isFighting = true;
        }
    }
    
    private void StopPlayerCart(float stoppingFactor)
    {
        // Save the speed of lookat and player to set it back to that value after the fight
        if (previousLookAtSpeed == 0)
        {
            previousLookAtSpeed = LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed;
            previousPlayerSpeed = dollycart.GetComponent<CinemachineDollyCart>().m_Speed;
        }
        //Debug.Log("stopping");
        dollycart.GetComponent<CinemachineDollyCart>().m_Speed = Mathf.Lerp(dollycart.GetComponent<CinemachineDollyCart>().m_Speed, 0, stoppingFactor);
        if (dollycart.GetComponent<CinemachineDollyCart>().m_Speed <= 0.1f)
            dollycart.GetComponent<CinemachineDollyCart>().m_Speed = 0f;

        LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed = Mathf.Lerp(LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed, 0, stoppingFactor);
        if (LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed <= 0.1f)
            LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed = 0f;
    }

    private void StartPlayerCart(float startingFactor)
    {
        //Debug.Log("starting");
        dollycart.GetComponent<CinemachineDollyCart>().m_Speed = Mathf.Lerp(dollycart.GetComponent<CinemachineDollyCart>().m_Speed, previousPlayerSpeed, startingFactor);
        LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed = Mathf.Lerp(LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed, previousLookAtSpeed, startingFactor);
        if (dollycart.GetComponent<CinemachineDollyCart>().m_Speed >= previousPlayerSpeed - 0.5f && LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed >= previousLookAtSpeed - 0.5f)
        {
            dollycart.GetComponent<CinemachineDollyCart>().m_Speed = previousPlayerSpeed;
            LookAtObj.GetComponent<CinemachineDollyCart>().m_Speed = previousLookAtSpeed;
            
        }
        return;


    }

    private void ChangeLookAtPos()
    {
        Vector3 heading = Vector3.zero;
        Vector3 direction = Vector3.zero;

        float defaultMoveStep = 0.1f;

        if (enemies.Count != 0)
        {
            float maxDist = Vector3.Distance(vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position, enemies[0].transform.position);

            newDistance = Vector3.Distance(dollycart.transform.position, vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position);

            if(newDistance < (Vector3.Distance(dollycart.transform.position, enemies[0].transform.position)))
            {
                // MOVE LOOKAT SLIGHTLY AWAY FROM PLAYER
                heading = vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position - transform.position;
                if (heading != Vector3.zero & newDistance != 0f)
                {
                    direction = heading / newDistance;
                }
            }
            else
            {
                direction = Vector3.Lerp(direction, Vector3.zero, 0.01f);
            }
            vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position = Vector3.MoveTowards(vCam.GetComponent<CinemachineVirtualCamera>().m_LookAt.position, (enemies[0].transform.position + new Vector3(0f,0.5f,0f) + (direction * 20)), (defaultMoveStep + maxDist)/10);
        }
        else
        {
            return;
        }
    }
}
