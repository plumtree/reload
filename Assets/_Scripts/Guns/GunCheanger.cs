﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GunCheanger : MonoBehaviour
{

    public GuiManager gui;

    public int selectedGun = 0;

    void Start()
    {
        SwitchGun();
    }

    void Update()
    {
        int previousGun = selectedGun;
        
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) {
            if (selectedGun >= transform.childCount - 1)
            {
                selectedGun = 0;
            }
            else
            {
                selectedGun++;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            if (selectedGun <= 0 )
            {
                selectedGun = transform.childCount - 1;
            }
            else
            {
                selectedGun--;
            }
        }
        if (previousGun != selectedGun) {
            SwitchGun();
        }
    }

    private void SwitchGun()
    {
        int i = 0;
        foreach (Transform weapon in transform)
        {
            if (i == selectedGun) {
                weapon.gameObject.SetActive(true);
                Debug.Log("Active Weapon: " + weapon.gameObject.name);
                gui.WeaponLabel.text = ("Weapon: " + weapon.gameObject.name);
            } else {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }
    }
}
