﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour
{
    public GuiManager gui;

    public Camera fpsCamera;
    public int Demage;
    public int MaxAmmo;
    public int SupplyAmmo;
    public bool infiniteAmmo;

    private int Ammo;
    

    int layerMask = 7;

    void Start()
    {
        Ammo = MaxAmmo;
        if (infiniteAmmo)
        {
            SupplyAmmo = 999;
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (Ammo > 0) {
                Shoot();
            }
            else {
                //sound of empty mag
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            Reload();
        }
        GuiShowAmmo();
    }

        public void Reload() { 
        int HowMuchToReload = MaxAmmo - Ammo;
        if (SupplyAmmo > 0)
        {
            if (SupplyAmmo - HowMuchToReload > 0)
            {
                Ammo += HowMuchToReload;
                SupplyAmmo -= HowMuchToReload;
            }
            else
            {
                Ammo += SupplyAmmo;
                SupplyAmmo = 0;
            }
            if (infiniteAmmo)
            {
                SupplyAmmo = 999;
            }
        }
    }

    public void Shoot()
    {
        RaycastHit hit;
        Ray ray = fpsCamera.ScreenPointToRay(Input.mousePosition);
        
        //Jakubs' test
        RaycastHit hit1;
        Ray ray1 = fpsCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit, 100, ~layerMask, QueryTriggerInteraction.Ignore) && hit.collider.gameObject.GetComponent<Enemy>())
        {
            //Vector3 dir = (hit.transform.position - hit.point).normalized;
            Debug.DrawRay(gameObject.transform.position, ray.direction * hit.distance, Color.green, 1000f);
            Debug.Log("hit on enemie. Demage given: " + Demage);
            //TransformationTest(hit, speed);
            DealDemageToEnemy(hit, Demage);
        }

        //Jakub's test
        if (Physics.Raycast(ray1, out hit1, 100, -1, QueryTriggerInteraction.Ignore) && !hit1.collider.gameObject.GetComponent<Enemy>())
        {
            Debug.DrawRay(gameObject.transform.position, ray.direction * hit1.distance, Color.red, 1000f);
            Debug.Log("You hit: " + hit1.collider.name);
        }
        Ammo-=1;   
    }

    private void DealDemageToEnemy(RaycastHit hit, int demage)
    {
        hit.collider.gameObject.GetComponent<Enemy>().DealDemage(demage);
    }

    public void GuiShowAmmo() {
        if (infiniteAmmo)
        {
            gui.AmmoLabel.text = "Ammo: " + Ammo + "/" + MaxAmmo;
        }
        else
        {
            gui.AmmoLabel.text = "Ammo: " + Ammo + "/" + MaxAmmo + " | Supply: " + SupplyAmmo;
        }
    }
}
