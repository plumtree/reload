﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBob : MonoBehaviour {

    [SerializeField] private float timer = 0f;
    [SerializeField] float bobbingSpeed = 0.18f;
    [SerializeField] float bobbingAmount = 0.2f;
    [SerializeField] float midpoint = 2f;

    private void Update()
    {
        float waveslice = 0f;

        float horizontal = 1f;
        float vertical = 1f;

        //horizontal input
        //vertical input

        Vector3 cSharpConversion = transform.localPosition;

        if (Mathf.Abs(horizontal) == 0 && Mathf.Abs(vertical) == 0)
        {
            timer = 0f;
        }
        else
        {
            waveslice = Mathf.Sin(timer);
            timer += bobbingSpeed;
            if (timer > Mathf.PI * 2)
            {
                timer -= (Mathf.PI * 2);
            }
        }
        if (waveslice != 0)
        {
            float translateChnage = waveslice * bobbingSpeed;
            float totalAxes = Mathf.Abs(horizontal) + Mathf.Abs(vertical);
            totalAxes = Mathf.Clamp(totalAxes, 0f, 1f);
            translateChnage = totalAxes * translateChnage;
            cSharpConversion.y = midpoint + translateChnage;
            cSharpConversion.x = midpoint + (translateChnage / 2);
        }
        else
        {
            cSharpConversion.y = midpoint;
            cSharpConversion.x = midpoint;
        }
        transform.localPosition = cSharpConversion;
    }
}
