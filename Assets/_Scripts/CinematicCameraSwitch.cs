﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinematicCameraSwitch : MonoBehaviour {

    [SerializeField]
    GameObject cinematicLookAt;
    [SerializeField]
    GameObject dollycart;
    [SerializeField]
    GameObject defaultLookAtObj;
    [SerializeField]
    GameObject defaultCam;
    [SerializeField]
    GameObject cinematicCam;

    private bool isCinematic;
    private bool isColliding;

    void Start () {
        isCinematic = false;

    }
	
	void Update ()
    {
		if(isColliding == true)
        {
            CheckPath();
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (isColliding) return;

        isColliding = true;
        Debug.Log("enter");
        if (other.tag == "Player")
        {
            //Debug.Log("test");
            StopCamera();

            //GetComponent<BoxCollider>().enabled = !GetComponent<BoxCollider>().enabled;

            defaultCam.GetComponent<CinemachineVirtualCamera>().m_Priority = 1;
            cinematicCam.GetComponent<CinemachineVirtualCamera>().m_Priority = 100;
            cinematicLookAt.GetComponent<CinemachineDollyCart>().m_Speed = 1;

            isCinematic = true;
        }
    }

    

    private void StopCamera()
    {
        Debug.Log("stop");
        dollycart.GetComponent<CinemachineDollyCart>().m_Speed = 0;
        defaultLookAtObj.GetComponent<CinemachineDollyCart>().m_Speed = 0;
    }

    private void StartCamera()
    {
        Debug.Log("start");
        dollycart.GetComponent<CinemachineDollyCart>().m_Speed = 0.5f;
        defaultLookAtObj.GetComponent<CinemachineDollyCart>().m_Speed = 0.5f;
    }

    private void CheckPath()
    {
        if (cinematicLookAt.GetComponent<CinemachineDollyCart>().m_Position < cinematicLookAt.GetComponent<CinemachineDollyCart>().m_Path.MaxPos && cinematicCam.GetComponent<CinemachineVirtualCamera>().m_Priority == 100)
        {
           // Debug.Log("position" + cinematicLookAt.GetComponent<CinemachineDollyCart>().m_Position);
        }
        else if (cinematicLookAt.GetComponent<CinemachineDollyCart>().m_Position >= cinematicLookAt.GetComponent<CinemachineDollyCart>().m_Path.MaxPos && cinematicCam.GetComponent<CinemachineVirtualCamera>().m_Priority == 100 && isColliding == true)
        {
            defaultCam.GetComponent<CinemachineVirtualCamera>().m_Priority = 100;
            cinematicCam.GetComponent<CinemachineVirtualCamera>().m_Priority = 1;
            isColliding = false;
            Debug.Log("test");
            StartCamera();
        }
    }
}
